﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
[assembly: ExportFont("johnnytorch.ttf")]
namespace tadekboard
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        
        private void _01click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr1();
        }
        private void _02click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr2();
        }
        private void _03click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr3();
        }
        private void _04click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr4();
        }
        private void _05click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr5();
        }
        private void _06click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr6();
        }
        private void _07click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr7();
        }
        private void _08click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr8();
        }
        private void _09click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr9();
        }
        private void _10click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr10();
        }
        private void _11click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr11();
        }
        private void _12click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr12();
        }
        private void _13click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr13();
        }
        private void _14click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr14();
        }
        private void _15click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr15();
        }
        private void _16click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr16();
        }
        private void _17click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr17();
        }
        private void _18click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().twr18();
        }
        private void _19click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().playFirst();
        }
        private void _20click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().playSecond();
        }
        private void _21click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().playThird();
        }
        private void _22click(object sender, EventArgs e)
        {
            DependencyService.Get<Interface1>().playFourth();
        }
    }
}
